import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {
  // State hooks to store the values of the input fields
  // getters are variables that store data (from setters)
  // setters are function that sets the data (for the getters)
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [mobile, setMobile] = useState('');
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);
  // State to indicate whether registration was successful or not
  const [registrationSuccess, setRegistrationSuccess] = useState(false);

  console.log(email);
  console.log(password1);
  console.log(password2);

  // Function to simulate user registration
  async function registerUser(e) {
    // Prevents page redirection via form submission
    e.preventDefault();

    const addUser = {
      firstName,
      lastName,
      email,
      mobileNo: mobile,
      password: password1,
    };

    const response = await fetch('http://localhost:4000/users/checkEmail', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email }),
    });

    const isDuplicate = await response.json();

    if (isDuplicate) {
      Swal.fire({
        title: 'Duplicate Email Found',
        icon: 'error',
        text: `${email} already exists`,
      });
    } else {
      fetch('http://localhost:4000/users/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(addUser),
      })
        .then((result) => result.json())
        .then((response) => {
          Swal.fire({
            title: 'Registration Successful',
            icon: 'success',
            text: 'Welcome to Zuitt!',
          });
          setRegistrationSuccess(true); // update the registrationSuccess state
        });
    }

    // Clear input fields
    setFirstName('');
    setLastName('');
    setMobile('');
    setEmail('');
    setPassword1('');
    setPassword2('');
  }

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (
      firstName &&
      firstName.trim() !== '' &&
      lastName &&
      lastName.trim() !== '' &&
      mobile.length > 10 &&
      email !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobile, email, password1, password2]);

  // Redirect to login page after successful registration
  if (registrationSuccess) {
    return <Navigate to="/login" />;
  }

  return (
    // 2-way Binding (Bnding the user input states into their corresponding input fields via the onChange JSX Event Handler)
    <Form onSubmit={registerUser}>
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Mobile No"
          value={mobile}
          onChange={(e) => setMobile(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
